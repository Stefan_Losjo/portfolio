# -*- coding: cp1252 -*-
# Programmeringsteknik webbkurs KTH inl�mningsuppgift 3.
# Stefan Losj�
# 2008 09 30
# L�ter programmets anv�ndare �ka attraktioner i en virtuellt n�jespark

from random import *

# Klassen attraktion beskriver attraktionerna p� n�jesf�ltet
class Attraktion:
    def __init__(self, namn, mini_tallness, magpirrsfaktor, attraktionsljud,
                 haveriljud, efterfras):
        self.namn = namn
        self.mini_tallness = mini_tallness
        self.magpirrsfaktor = magpirrsfaktor
        self.attraktionsljud = attraktionsljud
        self.haveriljud = haveriljud
        self.efterfras = efterfras

# K�r attraktionen och skriver ut den efterfras som finns till varje attraktion
# samt anv�ndarens nuvarande magpirrsfaktor
    def run_attraction(self, tallness, magpirrsfaktor):
        if tallness < self.mini_tallness:
            print "Jag �r ledsen, men v�ra sponsorer kr�ver att Ni �r allra \
minst", self.mini_tallness, "cm l�ng \n\
f�r att f� �ka den h�r attraktionen, ber s� hemskt mycket om urs�kt."
        else:
            intaktchans = random()
            if intaktchans >= 0.4:
                print self.attraktionsljud
                magpirrsfaktor += self.magpirrsfaktor
            else:
                print self.haveriljud
            print self.efterfras, "Er magpirrsfaktor �r nu: ", magpirrsfaktor
        return magpirrsfaktor
# --> def hantera_val() --> magpirrsfaktor = hantera_val(val)

#          ----------------------H�r slutar klassen----------------------

# Skriver ut h�lsningsfrasen
def write_welcome():
    attraktioner = [Attraktion("berg-och-dalbana", 145, 50, "\nAAAH! WHOOOSH",
                               """
KNIRK, SPROINK!

�h nej! Bergodalbanan har havererat!
�hm, ni f�r ett gratis �k som kompensation, s� har det h�r aldrig h�nt, OK?""",
                               """
S�g, var inte det n�gonting ut�ver det vanliga?"""),
                    Attraktion("flumeride", 100, 40, "\nIIIH! PLASK!!!",
                               """
BRAK, FORSSS!

Aj d�... Det gick h�l p� vattenr�nnan.
H�r, ta en bl�t sockervadd. Det bjuder vi p�, helt gratis!""",
                               "\nBl�tt �r s�tt, inte sant?"),
                    Attraktion("clownt�ltet", 5, 20, """
HAHAHA, HOHOHO, HIRR, HIRR, HIRR""",
                               """
hahaSKRIIIK! BRAK, FLUMP!

hmmm... jag kanske borde se till den ruttna mittstolpen... eller inte.
Urs�kta k�re Kund jag skall genast l�ta piska clownerna.
Rutten mittstolpe, det �r ju livsfarligt! Var s� god och g� till receptionen
s� f�r ni ett rabattkort p� en synvinkel, naturligtvis.""",
                               """
Hade inte Ni ocks� sv�rt att h�lla er? Inte...?
Nejnej, sj�lvklart inte. F�rl�t mig.""")]

    print """V�lkommen till SPR�TTEN p� toalettens N�jesf�lt!
V�lj med tangenterna 'b', 'f' och 'c'
mellan v�r nervpirrande berg-och-dalbana
helt byggd av ben fr�n medeltida vietnamesiska fullblodsdv�rgar,
eller kanske en kraftfull flumeride�ktur i en av v�ra unika farkoster
gjorda av urholkade enter, ja gott folk, s� �r sanningen. V�ldigt dyra.

I v�rt urval hittar ni ocks� en skrattupplevelse st�rre och starkare
�n n�gonting ni upplevt tidigare, clownt�ltet med de v�rldsber�mda �ttlingarna
Humbabas. En underh�llningsgrupp uteslutande best�ende av indianer fr�n en
hittills ok�nd stam i det innersta av Anderna som f�ster h�gsta status vid
att tillfredst�lla m�nniskor av v�r h�gre st�ende civilisation!"""
    return attraktioner

# L�ser in hur l�ng anv�ndaren utger sig f�r att vara och kontrollerar rimlighet
def read_tallness():
    while tallness_verification == False:
        tallness = input("""F�rl�t mig f�r att jag ber er \
om denna of�rl�tliga tj�nst,
f�r jag be om er l�ngd i cm skrivna med siffror, min mest framst�ende Kund. """)
        print
        if tallness > 240:
            print """Jag urs�ktar mig � det gr�vsta, \
men s� kan v�l inte vara fallet?
Jag m�ste ha grus i �ronen... *gr�va gr�va*
S�ja, skriv in er l�ngd i cm igen s� kanske mina helt klart
underl�gsna �ron denna g�ng lyckas h�ra vad Ni s�ger mig."""
        elif tallness == None:
            print """Jag �r helt f�rst�rd, \
men min plikt s�ger att jag m�ste veta.
Jag hoppas fr�n djupet av mitt hj�rta att jag
skall kunna gottg�ra Er f�r denna ol�genheten."""
        elif tallness < 5:
            print """Vad �r det Ni s�ger? �r Ni inte f�dd �nnu? \
Det pl�gar mig att s�ga Er detta
men jag har inga attraktioner f�r blivande foster.
Det �r fruktansv�rt men mina sponsorer tar tyv�rr inte foster som giltig
anledning att skjuta till medel att bygga ut n�jesparken med, hemskt ledsen.
Kanske Ni med Er storhet kan f� dem p� andra tankar?
"""
            return False, False
        else:
            print "Tackar djupast."
            return tallness, True
            

# Ger anv�ndaren valet av vilken attraktion den vill �ka i forts�ttningen
def ge_val():
    val = raw_input("'b' f�r berg-och-dalbanan, 'f' f�r flumeriden och 'c' \
f�r clownt�ltet. \nVar s� god, allra vackraste Kund, vad vill ni �ka? ")
    return val.lower()

# V�ljer r�tt attraktion eller skriver ut en urs�kt om att det inte f�rstod
def hantera_val(val):
    print
    if val == "b":
        return attraktioner[0].run_attraction(tallness, magpirrsfaktor)
    elif val == "f":
        return attraktioner[1].run_attraction(tallness, magpirrsfaktor)
    elif val == "c":
        return attraktioner[2].run_attraction(tallness, magpirrsfaktor)
    elif val =="inget":
        return magpirrsfaktor
    else:
        print "Urs�kta mig s� heeeemskt mycket,\n\
men jag lyckades inte riiiktigt f�rst� vad Ni sade."
        return magpirrsfaktor

# Skriver ut farv�lfrasen
def write_bye():
    print "Farv�l, och v�lkommen �ter, v�r allra b�sta Kund!"

#              ----------------Huvudprogrammet b�rjar----------------

magpirrsfaktor = 0
tallness_verification = val = False
attraktioner = write_welcome()
print "Urs�kta att jag fr�gar, men det kr�vs av mig att jag tar reda p� det."
tallness, tallness_verification = read_tallness()
while tallness_verification == True and val != "inget":
    val = ge_val()
    magpirrsfaktor = hantera_val(val)
    if magpirrsfaktor >= 300:
        print """
Var f�rsiktig, m�nga klarar inte mer �n 300 i magpirrsfaktor.
Tag mitt ringa r�d och kom tillbaka f�r att spendera mer senare."""
        break
write_bye()
