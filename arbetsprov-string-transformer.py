#!/usr/bin/python
# -*- coding: utf-8 -*-

"""
A method taking a string as input, substituting all 'e' for "ouch" and
'q' for "mourn". All 'u' are substituted according to following:

* 'u' preceded by 'l', 'L', 'r' or 'R' turns into "city"
* 'u' preceded by a consonant turns into "audacity"
* 'u' preceded by any vowel except for 'e' or 'E' turns into 'k'
* 'u' preceded by something other than a vowel or consonant turns into "luck"

Example input string:
"u e q lu ju iu eu u" transforms into
"luck ouch mourn lcity jaudacity ik ouchu luck"

A Swedish alphabet without special characters like 'é' or 'û' is assumed.

Input: input_str - A string
Output: output_str - A string
"""
def transform_string(input_str):
    SUBSTITUTIONS = ["audacity", "luck", "k", "city", "mourn", "ouch"]
    CONSONANTS = "qQwWrRtTpPsSdDfFgGhHjJkKlLzZxXcCvVbBnNmM"
    VOWELS = "eEuUiIoOåÅäÄöÖaAyY"
    VOWELS_NOT_E = VOWELS.replace("eE", "")

    #if (type(input_str) != <Class_string>) # TODO Control input correctness
    

    # Arbitrary long string. For speed, create list of strings. Later join
    #	it together.
    output_str_list = list(input_str)
    
    
    # Substitute 'u' first to not change substrings changed into containing 'u'
    
    # Find all indices containing 'u'
    indices_with_u = [pos for pos, char in enumerate(output_str_list)
    				  if char == 'u']

    start = 0
    # First char is 'u' -> list out of bounds
    if (0 in indices_with_u):
        # No character preceding is a special case
        output_str_list[0] = SUBSTITUTIONS[1]
        # Skip index 0 in rest of substituting
        start = 1
        
    for i in indices_with_u[start:]:
        if (input_str[i - 1] in "lLrR"):
            output_str_list[i] = SUBSTITUTIONS[3]
        elif (input_str[i - 1] in CONSONANTS):
            output_str_list[i] = SUBSTITUTIONS[0]
        elif (input_str[i - 1] in VOWELS_NOT_E):
            output_str_list[i] = SUBSTITUTIONS[2]
        # Redundant comparison for clarity and developability
        elif (input_str[i - 1] not in CONSONANTS and
              input_str[i - 1] not in VOWELS):
            output_str_list[i] = SUBSTITUTIONS[1]
    
    # Join string list into a string. Rest of substitution is faster on a string
    output_str = "".join(output_str_list)
    
    # The rest
    output_str = output_str.replace('e', SUBSTITUTIONS[5])
    output_str = output_str.replace('q', SUBSTITUTIONS[4])
    
    return output_str

input_str = input("Please input a text to be transformed "
				  + "(within quotes if you use Python 2): ")
print("Input :", input_str)
print("Answer:", transform_string(input_str))
